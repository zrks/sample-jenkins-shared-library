def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    pipeline {
        // our complete declarative pipeline can go in here
        agent none

        stages {
            stage('Build') {
                agent { label 'master' }

                steps {
                    script {
                        sh "echo " + config.test_me
                    }
                }
            }
        }
    }   
}
