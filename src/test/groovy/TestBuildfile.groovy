import org.jenkinsci.plugins.workflow.libs.GlobalLibraries
import org.jenkinsci.plugins.workflow.libs.LibraryConfiguration
import org.jenkinsci.plugins.workflow.libs.LibraryRetriever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.jvnet.hudson.test.JenkinsRule
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition
import org.jenkinsci.plugins.workflow.job.WorkflowJob

class TestBuildfile {

    @Rule
    public JenkinsRule jenkinsRule = new JenkinsRule()

    @Before
    void configureRule() {
        jenkinsRule.timeout = 30

        final LibraryRetriever retriever = new LocalLibraryRetriever()
        final LibraryConfiguration localLibrary = new LibraryConfiguration('testLibrary', retriever)

        localLibrary.implicit = true
        localLibrary.defaultVersion = 'unused'
        GlobalLibraries.get().setLibraries(Collections.singletonList(localLibrary))
    }

    @Test
    void testExecuteWithoutErrors() {

        String fileContents = new File('/tmp/testJenkinsfile').text
        final CpsFlowDefinition flow = new CpsFlowDefinition(fileContents, true)


        final WorkflowJob workflowJob = jenkinsRule.jenkins.createProject(WorkflowJob, 'project')
        workflowJob.definition = flow

        jenkinsRule.assertLogContains('yo', jenkinsRule.buildAndAssertSuccess(workflowJob))
        jenkinsRule.buildAndAssertSuccess(workflowJob)
    }

}