import hudson.FilePath
import org.jenkinsci.plugins.workflow.libs.LibraryRetriever
import hudson.model.Run
import hudson.model.TaskListener

import javax.annotation.Nonnull


class LocalLibraryRetriever extends LibraryRetriever {

    private final FilePath dir

    LocalLibraryRetriever() {
        this(new FilePath(new File(".")))
    }

    LocalLibraryRetriever(final FilePath dir) {
        this.dir = dir
    }

    @Override
    void retrieve(
            @Nonnull String name,
            @Nonnull String version,
            @Nonnull FilePath target,
            @Nonnull Run<?, ?> run,
            @Nonnull TaskListener listener) throws Exception {

        dir.copyRecursiveTo("src/**/*.groovy,vars/*.groovy,vars/*.txt,resources/", null, target)

    }

}
